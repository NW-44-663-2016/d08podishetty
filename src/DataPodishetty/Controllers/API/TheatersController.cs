using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DataPodishetty.Models;

namespace DataPodishetty.Controllers
{
    [Produces("application/json")]
    [Route("api/Theaters")]
    public class TheatersController : Controller
    {
        private AppDbContext _context;

        public TheatersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Theaters
        [HttpGet]
        public IEnumerable<Theater> GetTheaters()
        {
            return _context.Theaters;
        }

        // GET: api/Theaters/5
        [HttpGet("{id}", Name = "GetTheater")]
        public IActionResult GetTheater([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);

            if (theater == null)
            {
                return HttpNotFound();
            }

            return Ok(theater);
        }

        // PUT: api/Theaters/5
        [HttpPut("{id}")]
        public IActionResult PutTheater(int id, [FromBody] Theater theater)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != theater.TheatreId)
            {
                return HttpBadRequest();
            }

            _context.Entry(theater).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TheaterExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Theaters
        [HttpPost]
        public IActionResult PostTheater([FromBody] Theater theater)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Theaters.Add(theater);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TheaterExists(theater.TheatreId))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetTheater", new { id = theater.TheatreId }, theater);
        }

        // DELETE: api/Theaters/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTheater(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);
            if (theater == null)
            {
                return HttpNotFound();
            }

            _context.Theaters.Remove(theater);
            _context.SaveChanges();

            return Ok(theater);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TheaterExists(int id)
        {
            return _context.Theaters.Count(e => e.TheatreId == id) > 0;
        }
    }
}